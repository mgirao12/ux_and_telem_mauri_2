package com.dji.mobile_sdk.BaseClasses;

import android.util.Log;

import com.dji.mobile_sdk.EventDispatcherUtils.ParsedEventDispatcher.ParseEventListener;
import com.dji.mobile_sdk.EventDispatcherUtils.ParsedEventDispatcher.ParsedEventDispatcher;
import com.dji.mobile_sdk.EventDispatcherUtils.genericDispatcher.EventDispatcher;
import com.dji.mobile_sdk.Generics.IPacket;
import com.dji.mobile_sdk.mqtt.MQTTConnection;
import com.dji.mobile_sdk.mqtt.utils.MqttSubscriptionCallbacks;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.paho.client.mqttv3.IMqttMessageListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

public class Communicator {

    private static final Logger log = LogManager.getLogger(Communicator.class);

    private MQTTConnection mqttConnection;
    private ConcurrentHashMap<String, ParsedEventDispatcher> dispatchers = new ConcurrentHashMap<>();
    private ConcurrentHashMap<String, Integer> typeCounters = new ConcurrentHashMap<>();

    /**
     * Default constructor for enabled radio
     */
    public Communicator() {
        mqttConnection = new MQTTConnection();
    }

    public void start() {
        mqttConnection.start();
    }

    public void unsubscribe(String systemID, String type) throws MqttException, IOException {
        mqttConnection.getMqttAsyncClient().unsubscribe(mqttConnection.getTopic(type, systemID));
        for (IMqttToken mqttListener : mqttConnection.getMqttListenersList()) {
            if (mqttListener.getTopics()[0].equals(mqttConnection.getTopic(type, systemID))) {
                mqttConnection.getMqttListenersList().remove(mqttListener);
                break;
            }
        }

    }

    public EventDispatcher<IMqttToken> subscribe(String systemID, String type, ParseEventListener listener) throws MqttException, IOException {
        ParsedEventDispatcher dispatcher;
        EventDispatcher<IMqttToken> externalDispatcher = new EventDispatcher<>();

        if (dispatchers.containsKey(type)) {
            dispatcher = dispatchers.get(type);
        } else {
            dispatcher = new ParsedEventDispatcher();
            dispatchers.put(type, dispatcher);
        }
        dispatcher.registerListener(listener);
        IMqttToken subscribeToken = mqttConnection.getMqttAsyncClient().subscribe(mqttConnection.getTopic(type, systemID), 0, null, new MqttSubscriptionCallbacks(externalDispatcher), new MQTTListener(dispatcher));
        mqttConnection.getMqttListenersList().add(subscribeToken);
        return externalDispatcher;

    }

    public void publish(String systemID, String type, IPacket packet) throws IOException, JSONException {


        if (typeCounters.containsKey(type)) {
            typeCounters.put(type, typeCounters.get(type) + 1);
        } else {
            typeCounters.put(type, 0);
        }
        JSONObject object = packet.toJSON();
        object.put("msgID", packet.getMsgID()); //Xq pone esto? Si ya esta
        //object.replaceAll("NaN", -1 /*Or whatever you need*/);
        Log.e("MQTT:", "dentro de PUBLISH");
        try{
            mqttConnection.publishMessage(mqttConnection.getTopic(type, systemID), object.toString(), null, 0, false);
        } catch (Exception e) {
            Log.e("ERROR PUBLICAR MQTT", "NO VA " + e);
            e.printStackTrace();
        }
    }
        //mqttConnection.publishMessage("Topic", object.toString(), null, 0, false);

    public void stop() throws MqttException {
        IMqttToken token = mqttConnection.getMqttAsyncClient().disconnect();
        int count = 0;
        while (count++ < 5) {
            if (token.isComplete()) {
                mqttConnection.getMqttAsyncClient().close();
                break;
            }
            try {
                Thread.sleep(2000);
            } catch (Exception e) {
                log.error(e);
            }
        }
        if (count > 5) {
            mqttConnection.getMqttAsyncClient().disconnectForcibly();
            mqttConnection.getMqttAsyncClient().close();
        }
    }

    public MQTTConnection getMQTTConnection() {
        return mqttConnection;
    }

    private class MQTTListener implements IMqttMessageListener {
        ParsedEventDispatcher dispatcher;

        public MQTTListener(ParsedEventDispatcher dispatcher) {
            this.dispatcher = dispatcher;
        }

        @Override
        public void messageArrived(String s, MqttMessage mqttMessage) {
            try {
                dispatcher.dispatch(new JSONObject(new String(mqttMessage.getPayload(), mqttConnection.getENCODING_FOR_PAYLOAD())));
            } catch (Exception e) {
                log.error(e);
            }

        }
    }

}

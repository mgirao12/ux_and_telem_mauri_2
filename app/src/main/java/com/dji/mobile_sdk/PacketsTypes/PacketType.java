package com.dji.mobile_sdk.PacketsTypes;

public class PacketType {
    final static public String ACK = "ACK";
    final static public String Heartbeat = "Heartbeat";
    final static public String Home = "Home";
    final static public String Flightplan = "Flightplan";
    final static public String LTEData = "LTEData";
    final static public String Telemetry = "Telemetry";
    final static public String DroneStatus = "DroneStatus";
    final static public String MissionStatus = "MissionStatus";
    final static public String Command = "Command";
    final static public String SystemStatus = "SystemStatus";
    final static public String RadiationData = "RadiationData";
}

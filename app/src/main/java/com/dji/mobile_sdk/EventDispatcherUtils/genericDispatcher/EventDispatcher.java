package com.dji.mobile_sdk.EventDispatcherUtils.genericDispatcher;


import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Class to register event listener and dispatch events
 */

public class EventDispatcher<T> {

    List<EventListener<T>> listeners;

    public void registerListener(EventListener<T> listener) {
        if (listeners == null) {
            listeners = new CopyOnWriteArrayList<EventListener<T>>();
        }
        listeners.add(listener);
    }

    public void dispatch(T event) {
        if (listeners != null) {
            if (event != null) {
                for (EventListener<T> listener : listeners) {
                    listener.onEvent(event);
                }
            }
        }
    }

    public void unregister(EventListener<T> listener) {
        listeners.remove(listener);
    }
}
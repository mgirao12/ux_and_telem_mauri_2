package com.dji.mobile_sdk.EventDispatcherUtils.genericDispatcher;

/**
 * Interface that should be implemented by any event listener
 */
public interface EventListener<T> {
    void onEvent(T eventObject);
}
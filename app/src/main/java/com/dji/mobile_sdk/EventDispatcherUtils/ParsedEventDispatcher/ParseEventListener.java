package com.dji.mobile_sdk.EventDispatcherUtils.ParsedEventDispatcher;

import org.json.JSONObject;

/**
 * Interface that should be implemented by any event listener
 */
public interface ParseEventListener {
    void onEvent(Object source, JSONObject JSON);
}
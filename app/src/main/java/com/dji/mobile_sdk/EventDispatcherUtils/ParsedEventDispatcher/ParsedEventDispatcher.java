package com.dji.mobile_sdk.EventDispatcherUtils.ParsedEventDispatcher;


import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Class to register event listener and dispatch events
 */

public class ParsedEventDispatcher {

    List<ParseEventListener> listeners;
    Pair<Integer, Integer> pair;

    public void registerListener(ParseEventListener listener) {
        if (listeners == null) {
            listeners = new CopyOnWriteArrayList<ParseEventListener>();
        }
        listeners.add(listener);
    }

    public void dispatch(JSONObject event) {
        if (listeners != null) {
            if (event != null) {
                for (ParseEventListener listener : listeners) {
                    listener.onEvent(this, event);
                }
            }
        }
    }

    public void unregister(ParseEventListener listener) {
        listeners.remove(listener);
    }
}
package com.dji.mobile_sdk;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import com.dji.mobile_sdk.BaseClasses.Communicator;
import com.dji.mobile_sdk.PacketsTypes.PacketType;
import com.dji.mobile_sdk.utils.GlobalArea;
import com.dji.mobile_sdk.utils.LogUtils;
import com.dji.mobile_sdk.vehicle_connection.ConnectionActivity;
import com.dji.mobile_sdk.vehicle_connection.SDKHandler;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import dji.common.airlink.SignalQualityCallback;
import dji.common.error.DJIError;
import dji.common.error.DJIWaypointV2Error;
import dji.common.flightcontroller.FlightControllerState;
import dji.common.mission.waypoint.WaypointDownloadProgress;
import dji.common.mission.waypoint.WaypointExecutionProgress;
import dji.common.mission.waypoint.WaypointMissionDownloadEvent;
import dji.common.mission.waypoint.WaypointMissionExecutionEvent;
import dji.common.mission.waypoint.WaypointMissionState;
import dji.common.mission.waypoint.WaypointMissionUploadEvent;
import dji.common.mission.waypoint.WaypointUploadProgress;
import dji.common.mission.waypointv2.WaypointV2DownloadProgress;
import dji.common.mission.waypointv2.WaypointV2ExecutionProgress;
import dji.common.mission.waypointv2.WaypointV2MissionDownloadEvent;
import dji.common.mission.waypointv2.WaypointV2MissionExecutionEvent;
import dji.common.mission.waypointv2.WaypointV2MissionState;
import dji.common.mission.waypointv2.WaypointV2MissionUploadEvent;
import dji.common.mission.waypointv2.WaypointV2UploadProgress;
import dji.common.util.CommonCallbacks;
import dji.sdk.airlink.AirLink;
import dji.sdk.base.BaseProduct;
import dji.sdk.flightcontroller.FlightController;
import dji.sdk.mission.MissionControl;
import dji.sdk.mission.waypoint.WaypointMissionOperator;
import dji.sdk.mission.waypoint.WaypointMissionOperatorListener;
import dji.sdk.mission.waypoint.WaypointV2MissionOperator;
import dji.sdk.mission.waypoint.WaypointV2MissionOperatorListener;
import dji.sdk.products.Aircraft;
import dji.sdk.sdkmanager.DJISDKManager;


public class MainActivity extends FragmentActivity implements View.OnClickListener {

    private static final String TAG = ConnectionActivity.class.getName();

    private double droneLocationLat = -1, droneLocationLng = -1;
    private float droneRoll = -1, dronePitch=-1, droneYaw=-1;
    private float droneVelX=-1, droneVelY=-1, droneVelZ=-1, droneHeading=0;
    private long timestamp=0L;
    private long oldTs = 0L;
    private float oldDroneVelX=0, oldDroneVelY=0, oldDroneVelZ=0;
    private boolean droneFlying = false;
    private float droneLocationAlt = -1, takeoffLocationAlt = -1;;
    private FlightController mFlightController;
    private AirLink mAirlinkController;

    private Handler uiHandler;




    //EditText
    TextView latitudeTextView;
    TextView longitudTextView;
    TextView altitudeTextView;
    TextView rollTextView;
    TextView pitchTextView;
    TextView yawTextView;
    TextView velXTextView;
    TextView velYTextView;
    TextView velZTextView;
    TextView accelXTextView;
    TextView accelYTextView;
    TextView accelZTextView;
    TextView serialNumberTextView;
    TextView vehicleNameTextView;
    TextView timestampTextView;
    TextView headingTextView;
    TextView ATimeTextView;



    protected BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

        }
    };

    @Override
    protected void onResume(){
        super.onResume();
    }

    @Override
    protected void onPause(){
        super.onPause();
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        unregisterReceiver(mReceiver);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        uiHandler = new Handler(Looper.getMainLooper());

        latitudeTextView = (TextView) findViewById(R.id.latitude);
        longitudTextView = (TextView) findViewById(R.id.longitude);
        altitudeTextView = (TextView) findViewById(R.id.altitude);
        rollTextView = (TextView) findViewById(R.id.roll);
        pitchTextView =(TextView) findViewById(R.id.pitch);
        yawTextView = (TextView) findViewById(R.id.yaw);
        velXTextView = (TextView) findViewById(R.id.velX);
        velYTextView = (TextView) findViewById(R.id.velY);
        velZTextView = (TextView) findViewById(R.id.velZ);
        accelXTextView = (TextView) findViewById(R.id.accelX);
        accelYTextView = (TextView) findViewById(R.id.accelY);
        accelZTextView = (TextView) findViewById(R.id.accelZ);
        serialNumberTextView = (TextView) findViewById(R.id.serialNumber);
        vehicleNameTextView = (TextView) findViewById(R.id.vehicleName);
        timestampTextView = (TextView) findViewById(R.id.flightTime);
        headingTextView = (TextView) findViewById(R.id.heading);
        ATimeTextView = (TextView) findViewById(R.id.ATime);


        if(isNetworkAvailable()) {
            LogUtils.getInstance().writeLog(TAG, "Network Available");
            Toast.makeText(getApplicationContext(), "Network Available", Toast.LENGTH_LONG).show();

        } else {
            Toast.makeText(getApplicationContext(), "Network is not available yet", Toast.LENGTH_LONG).show();
        }

        Timer sendDjiInfoTOUI = new Timer();
        sendDjiInfoTOUI.schedule(new TimerTask() {
            @Override
            public void run() {
                affectUIThread();
            }
        }, 0, 1000);


        fillTelemetryDataToTextView();

        //PRUEBA DE PUBLICAR

        Communicator communicator = new Communicator();
        communicator.start();
        String systemId = "Drone1234";//name of the drone
        String packetType = PacketType.Telemetry;//type of packet, decides what message is sent

        Timer timerSuscriptor = new Timer ();
        timerSuscriptor.schedule(new TimerTask() {
            @Override
            public void run() {
                try {
                    communicator.publish(systemId, packetType, GlobalArea.getInstance());
                    Log.e("ROLL DEL DRON", String.valueOf(GlobalArea.getInstance().getDroneRoll()));
                    Log.e(TAG, "ENVIADO");
                    //showToast("ENVIADO");
                }catch (IOException | JSONException e){
                    System.out.println(e);
                    Log.e(TAG, "NO ENVIADO " + e);
                    showToast("NO ENVIADO");
                }
            }
        }, 10000, 250);




    }



    public void affectUIThread() {
        uiHandler.post(new Runnable() {
            @Override
            public void run() {

                serialNumberTextView.setText(GlobalArea.getInstance().getSerialNumber());
                vehicleNameTextView.setText(GlobalArea.getInstance().getTypeOfVehicle());
                latitudeTextView.setText(String.valueOf(GlobalArea.getInstance().getDroneLocationLat()));
                longitudTextView.setText(String.valueOf(GlobalArea.getInstance().getDroneLocationLng()));
                altitudeTextView.setText(String.valueOf(GlobalArea.getInstance().getDroneAltitude()));
                headingTextView.setText(Float.toString(GlobalArea.getInstance().getDroneHeading()));
                rollTextView.setText(String.valueOf(GlobalArea.getInstance().getDroneRoll()));
                pitchTextView.setText(String.valueOf(GlobalArea.getInstance().getDronePitch()));
                yawTextView.setText(String.valueOf(GlobalArea.getInstance().getDroneYaw()));
                timestampTextView.setText(String.valueOf(GlobalArea.getInstance().getTimestamp()));


                velXTextView.setText(Double.toString(GlobalArea.getInstance().getDroneVelX()));
                velYTextView.setText(Double.toString(GlobalArea.getInstance().getDroneVelY()));
                velZTextView.setText(Double.toString(GlobalArea.getInstance().getDroneVelZ()));

                GlobalArea.getInstance().setDroneAccelX(droneVelX-oldDroneVelX);
                accelXTextView.setText(Float.toString(GlobalArea.getInstance().getDroneAccelX()));
                oldDroneVelX=droneVelX;

                GlobalArea.getInstance().setDroneAccelY(droneVelY-oldDroneVelY);
                accelYTextView.setText(Float.toString(GlobalArea.getInstance().getDroneAccelY()));
                oldDroneVelY=droneVelY;

                GlobalArea.getInstance().setDroneAccelZ(droneVelZ-oldDroneVelZ);
                accelZTextView.setText(Float.toString(GlobalArea.getInstance().getDroneAccelZ()));
                oldDroneVelZ=droneVelZ;

                ATimeTextView.setText(Long.toString(timestamp-oldTs));
                oldTs=timestamp;
            }
        });
    }



    //Función para saber si la tablet tiene internet
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            default:
                break;
        }
    }

    private void fillTelemetryDataToTextView() {

        BaseProduct product = SDKHandler.getProductInstance();
        MissionControl mission = DJISDKManager.getInstance().getMissionControl();

        //Navegación V2 para vehiculos DJI mas modernos - por ejemplo M300RTK
        WaypointV2MissionOperator missionOperator = mission.getWaypointMissionV2Operator();

        if (product != null && product.isConnected()) {
            if (product instanceof Aircraft) {
                mFlightController = ((Aircraft) product).getFlightController();
                mAirlinkController = product.getAirLink();



                mFlightController.getSerialNumber(new CommonCallbacks.CompletionCallbackWith<String>() {
                    @Override
                    public void onSuccess(String serialNumber) {
                        GlobalArea.getInstance().setSerialNumber(serialNumber);
                        LogUtils.getInstance().writeLog(TAG, "Vehicle Serial Number is: " + serialNumber);

                    }
                    @Override
                    public void onFailure(DJIError djiError) {
                        showToast("Error obtaining serial number");
                        LogUtils.getInstance().writeLog("ERROR -- Cannot obtain serial number of vehicle...\n");
                    }
                });

                product.getName(new CommonCallbacks.CompletionCallbackWith<String>() {
                    @Override
                    public void onSuccess(String s) {
                        GlobalArea.getInstance().setTypeOfVehicle(s);
                    }

                    @Override
                    public void onFailure(DJIError djiError) {

                    }
                });
            }
        }

        WaypointMissionOperator waypointMission = mission.getWaypointMissionOperator();
        waypointMission.addListener(new WaypointMissionOperatorListener() {
            @Override
            public void onDownloadUpdate(@NonNull WaypointMissionDownloadEvent waypointMissionDownloadEvent) {
                WaypointDownloadProgress progress = waypointMissionDownloadEvent.getProgress();
                if(progress != null){
                    LogUtils.getInstance().writeLog("DJI MISSION DOWNLOAD. Total: " + progress.totalWaypointCount + " Last:" + progress.downloadedWaypointIndex);
                } else {
                    LogUtils.getInstance().writeLog("DJI MISSION DOWNLOAD.");
                }
            }

            @Override
            public void onUploadUpdate(@NonNull WaypointMissionUploadEvent waypointMissionUploadEvent) {
                WaypointUploadProgress progress = waypointMissionUploadEvent.getProgress();
                if(progress != null){
                    LogUtils.getInstance().writeLog("DJI MISSION UPLOAD. Total: " + progress.totalWaypointCount + " Last:" + progress.uploadedWaypointIndex);
                } else {
                    LogUtils.getInstance().writeLog("DJI MISSION UPLOAD.");
                }

            }

            @Override
            public void onExecutionUpdate(@NonNull WaypointMissionExecutionEvent waypointMissionExecutionEvent) {
                WaypointMissionState state = waypointMissionExecutionEvent.getCurrentState();
                WaypointExecutionProgress progress = waypointMissionExecutionEvent.getProgress();

                if(progress != null){
                    LogUtils.getInstance().writeLog("DJI MISSION PROGRESS. Target: " + progress.targetWaypointIndex + " Reached:" + progress.isWaypointReached);
                } else {
                    LogUtils.getInstance().writeLog("DJI MISSION PROGRESS.");
                }

            }

            @Override
            public void onExecutionStart() {
                LogUtils.getInstance().writeLog("DJI EXECUTION STARTS");
            }

            @Override
            public void onExecutionFinish(@Nullable DJIError djiError) {
                if(djiError != null){
                    LogUtils.getInstance().writeLog("DJI EXECUTION FINISHES WITH CODE: " + djiError.getDescription());
                } else {
                    LogUtils.getInstance().writeLog("DJI EXECUTION FINISHES.");
                }

            }
        });

        if (missionOperator != null){
            missionOperator.addWaypointEventListener(new WaypointV2MissionOperatorListener() {
                @Override
                public void onDownloadUpdate(@NonNull WaypointV2MissionDownloadEvent waypointV2MissionDownloadEvent) {
                    WaypointV2DownloadProgress progress = waypointV2MissionDownloadEvent.getProgress();
                    LogUtils.getInstance().writeLog("DJI MISSION DOWNLOAD. Total: " + progress.getTotalWaypointCount() + " Last:" + progress.getLastDownloadedWaypointIndex());
                }

                @Override
                public void onUploadUpdate(@NonNull WaypointV2MissionUploadEvent waypointV2MissionUploadEvent) {
                    WaypointV2UploadProgress progress = waypointV2MissionUploadEvent.getProgress();
                    LogUtils.getInstance().writeLog("DJI MISSION UPLOAD. Total: " + progress.getTotalWaypointCount() + " Last:" + progress.getLastUploadedWaypointIndex());
                }

                @Override
                public void onExecutionUpdate(@NonNull WaypointV2MissionExecutionEvent waypointV2MissionExecutionEvent) {
                    WaypointV2MissionState state = waypointV2MissionExecutionEvent.getCurrentState();
                    WaypointV2ExecutionProgress progress = waypointV2MissionExecutionEvent.getProgress();
                    LogUtils.getInstance().writeLog("DJI MISSION PROGRESS. Target: " + progress.getTargetWaypointIndex() + " Reached:" + progress.isWaypointReached());
                }

                @Override
                public void onExecutionStart() {
                    LogUtils.getInstance().writeLog("DJI EXECUTION STARTS");
                }

                @Override
                public void onExecutionFinish(@Nullable DJIWaypointV2Error djiWaypointV2Error) {
                    LogUtils.getInstance().writeLog("DJI EXECUTION FINISHES WITH CODE: " + djiWaypointV2Error.getDescription());
                }

                @Override
                public void onExecutionStopped() {
                    LogUtils.getInstance().writeLog("DJI EXECUTION STOPPED BY USER");
                }
            });
        }


        if (mAirlinkController != null) {
            mAirlinkController.setDownlinkSignalQualityCallback(new SignalQualityCallback() {
                @Override
                public void onUpdate(int quality){
                    LogUtils.getInstance().writeLog("DOWNLINK WIFI QUALITY: " + quality);
                }
            });

            mAirlinkController.setUplinkSignalQualityCallback(new SignalQualityCallback() {
                @Override
                public void onUpdate(int quality){
                    LogUtils.getInstance().writeLog("UPLINK WIFI QUALITY: " + quality);
                }
            });
        }

        if (mFlightController != null) {

            mFlightController.setStateCallback(new FlightControllerState.Callback() {
                @Override
                public void onUpdate(FlightControllerState djiFlightControllerCurrentState) {

                    timestamp = System.currentTimeMillis();
                    GlobalArea.getInstance().setTimestamp(timestamp);

                    droneLocationLat = djiFlightControllerCurrentState.getAircraftLocation().getLatitude();
                    //GlobalArea.getInstance().setDroneLocationLat(droneLocationLat); //ANULAMOS PORQUE DA NAN

                    droneLocationLng = djiFlightControllerCurrentState.getAircraftLocation().getLongitude();
                    //GlobalArea.getInstance().setDroneLocationLng(droneLocationLng); //ANULAMOS PORQUE DA NAN

                    droneLocationAlt = djiFlightControllerCurrentState.getAircraftLocation().getAltitude();
                    GlobalArea.getInstance().setDroneAltitude(droneLocationAlt);

                    droneHeading = mFlightController.getCompass().getHeading();
                    GlobalArea.getInstance().setDroneHeading(droneHeading);

                    droneRoll=(float) djiFlightControllerCurrentState.getAttitude().roll;
                    GlobalArea.getInstance().setDroneRoll(droneRoll);

                    dronePitch= (float) djiFlightControllerCurrentState.getAttitude().pitch;
                    GlobalArea.getInstance().setDronePitch(dronePitch);

                    droneYaw = (float) djiFlightControllerCurrentState.getAttitude().yaw;
                    GlobalArea.getInstance().setDroneYaw(droneYaw);

                    droneVelX=djiFlightControllerCurrentState.getVelocityX();
                    GlobalArea.getInstance().setDroneVelX(droneVelX);

                    droneVelY=djiFlightControllerCurrentState.getVelocityY();
                    GlobalArea.getInstance().setDroneVelY(droneVelY);

                    droneVelZ=djiFlightControllerCurrentState.getVelocityZ();
                    GlobalArea.getInstance().setDroneVelZ(droneVelZ);








                }
            });



            //Get Heading

            /*
            if (mFlightController != null) {

                mFlightController.getCompass().setCompassStateCallback(new CompassState.Callback() {
                    @Override
                    public void onUpdate(@NonNull CompassState compassState) {
                        headingTextView.setText(String.valueOf(mFlightController.getCompass().getHeading()));
                    }
                });

            }




            /*  "required": ["yaw", "pitch", "roll", "vectorSpeedZ", "vectorSpeedY","vectorSpeedX",
             "track", "timestamp","absoluteAltitude", "relativeAltitude", "latitude", "longitude",
            "accelerationX", "accelerationY", "accelerationZ", "systemId"]
             */




        }
    }

    private void showToast(final String toastMsg) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), toastMsg, Toast.LENGTH_LONG).show();
            }
        });
    }
}
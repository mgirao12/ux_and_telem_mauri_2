package com.dji.mobile_sdk.utils;

import com.dji.mobile_sdk.Generics.IDataPacket;

import org.json.JSONException;
import org.json.JSONObject;
import org.msgpack.core.MessageBufferPacker;
import org.msgpack.core.MessagePack;
import org.msgpack.core.MessageUnpacker;

import java.io.IOException;
import java.util.Iterator;

public class GlobalArea implements IDataPacket{

    private static volatile GlobalArea sSoleInstance;

    private final int msgid = MessageIDs.Telemetry; //si
    private double latitude = -1; //si
    private double longitude = -1; //si
    private float absoluteAltitude =-1; //si
    private float roll = -1; //si
    private float pitch =-1; //si
    private float yaw =-1; //si
    private float track=0; //si DONDE LO USO?
    private double vectorSpeedX =-1; //si
    private double vectorSpeedY =-1; //si
    private double vectorSpeedZ =-1; //si
    private float accelerationX =-1; //si
    private float accelerationY =-1; //si
    private float accelerationZ =-1; //si
    private float droneHeading=0; //FALTA?!?!
    private String serialNumber=""; //no sale
    private String TypeOfVehicle=""; //no sale
    private long timestamp=0L; //si
    private String systemId=""; //si DONDE USARLO?
    private String sessionId; //si
    private String operationId;//si

    private float relativeAltitude=0; //si pero no salia...

    public long getTimestamp() {
        return timestamp;
    }

    public synchronized void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public double getDroneAltitude() {
        return absoluteAltitude;
    }

    public synchronized void setDroneAltitude(float absoluteAltitude) {
        this.absoluteAltitude = absoluteAltitude;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public synchronized void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getTypeOfVehicle() {
        return TypeOfVehicle;
    }

    public synchronized void setTypeOfVehicle(String typeOfVehicle) {
        TypeOfVehicle = typeOfVehicle;
    }

    public double getDroneLocationLat() {
        return latitude;
    }

    public synchronized void setDroneLocationLat(double latitude) {
        this.latitude = latitude;
    }

    public double getDroneLocationLng() {
        return longitude;
    }

    public synchronized void setDroneLocationLng(double longitude) {
        this.longitude = longitude;
    }

    public double getDroneRoll() {
        return roll;
    }

    public synchronized void setDroneRoll(float roll) {
        this.roll = roll;
    }

    public double getDronePitch() {
        return pitch;
    }

    public synchronized void setDronePitch(float pitch) {
        this.pitch = pitch;
    }

    public double getDroneYaw() {
        return yaw;
    }

    public synchronized void setDroneYaw(float yaw) {
        this.yaw = yaw;
    }

    public double getDroneVelX() {
        return vectorSpeedX;
    }

    public synchronized void setDroneVelX(float vectorSpeedX) {
        this.vectorSpeedX = vectorSpeedX;
    }

    public double getDroneVelY() {
        return vectorSpeedY;
    }

    public synchronized void setDroneVelY(float vectorSpeedY) {
        this.vectorSpeedY = vectorSpeedY;
    }

    public double getDroneVelZ() {
        return vectorSpeedZ;
    }

    public synchronized void setDroneVelZ(float vectorSpeedZ) {
        this.vectorSpeedZ = vectorSpeedZ;
    }

    public float getDroneAccelX() {
        return accelerationX;
    }

    public synchronized void setDroneAccelX(float accelerationX) {
        this.accelerationX = accelerationX;
    }

    public float getDroneAccelY() {
        return accelerationY;
    }

    public synchronized void setDroneAccelY(float accelerationY) {
        this.accelerationY = accelerationY;
    }

    public float getDroneAccelZ() {
        return accelerationZ;
    }

    public synchronized void setDroneAccelZ(float accelerationZ) {
        this.accelerationZ = accelerationZ;
    }

    public float getDroneHeading() {
        return droneHeading;
    }

    public synchronized void setDroneHeading(float droneHeading) {
        this.droneHeading = droneHeading;
    }

    public String getSystemId() {
        return systemId;
    }

    public void setSystemId(String systemId) {
        this.systemId = systemId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getOperationId() {
        return operationId;
    }

    public void setOperationId(String operationId) {
        this.operationId = operationId;
    }

    public float getRelativeAltitude() {
        return relativeAltitude;
    }

    public void setRelativeAltitude(float relativeAltitude) {
        this.relativeAltitude = relativeAltitude;
    }



    //private constructor.
    private GlobalArea(){

        //Prevent form the reflection api.
        if (sSoleInstance != null){
            throw new RuntimeException("Use getInstance() method to get the single instance of this class.");
        }
    }

    public static GlobalArea getInstance() {
        //Double check locking pattern
        if (sSoleInstance == null) { //Check for the first time

            synchronized (GlobalArea.class) {   //Check for the second time.
                //if there is no instance available... create new one
                if (sSoleInstance == null) sSoleInstance = new GlobalArea();
            }
        }

        return sSoleInstance;
    }
    @Override
    public int getMsgID() {
        return msgid;
    }

    //FUNCIONES DE RIMMA COMMS

    @Override
    public byte[] pack() throws IOException {
        MessageBufferPacker packer = MessagePack.newDefaultBufferPacker();
        packer.packString(systemId);
        packer.packFloat(pitch);
        packer.packFloat(roll);
        packer.packFloat(yaw);
        packer.packFloat(track);
        packer.packFloat(accelerationX);
        packer.packFloat(accelerationY);
        packer.packFloat(accelerationZ);
        packer.packFloat(relativeAltitude);
        packer.packFloat(absoluteAltitude);
        packer.packDouble(latitude);
        packer.packDouble(longitude);
        packer.packDouble(vectorSpeedX);
        packer.packDouble(vectorSpeedY);
        packer.packDouble(vectorSpeedZ);
        packer.packLong(timestamp);
        return packer.toByteArray();

    }

    @Override
    public void unpack(byte[] data) throws IOException {
        MessageUnpacker packer = MessagePack.newDefaultUnpacker(data);
        systemId = packer.unpackString();
        pitch = packer.unpackFloat();
        roll = packer.unpackFloat();
        yaw = packer.unpackFloat();
        track = packer.unpackFloat();
        accelerationX = packer.unpackFloat();
        accelerationY = packer.unpackFloat();
        accelerationZ = packer.unpackFloat();
        relativeAltitude = packer.unpackFloat();
        absoluteAltitude = packer.unpackFloat();
        latitude = packer.unpackDouble();
        longitude = packer.unpackDouble();
        vectorSpeedX = packer.unpackDouble();
        vectorSpeedY = packer.unpackDouble();
        vectorSpeedZ = packer.unpackDouble();
        timestamp = packer.unpackLong();
        packer.close();
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject message = new JSONObject();
        message.put("msgID", msgid);
        message.put("track", track);
        message.put("relativeAltitude", relativeAltitude);
        message.put("absoluteAltitude", absoluteAltitude);
        message.put("latitude", latitude);
        message.put("longitude", longitude);
        message.put("speedX", vectorSpeedX);
        message.put("speedY", vectorSpeedY);
        message.put("speedZ", vectorSpeedZ);
        message.put("accX", accelerationX);
        message.put("accY", accelerationY);
        message.put("accZ", accelerationZ);
        message.put("roll", roll);
        message.put("yaw", yaw);
        message.put("pitch", pitch);
        message.put("timestamp", timestamp);
        message.put("systemId", systemId);
        return message;
    }

    public void fromString(String string) throws JSONException {
        JSONObject jsonObject = new JSONObject(string);
        Iterator<String> keys = jsonObject.keys();
        //Cycle through all keys
        while (keys.hasNext()) {
            //Selecting next key in the iterator
            String key = keys.next();
            //Reading the json for the key, and writing the variable for the case.
            switch (key) {
                case "pitch": //NO VAN LOS GET FLOATS!!!
                    //pitch = jsonObject.getFloat("pitch");
                    break;
                case "roll":
                    //roll = jsonObject.getFloat("roll");
                    break;
                case "yaw":
                    //yaw = jsonObject.getFloat("yaw");
                    break;
                case "track":
                   //track = jsonObject.getFloat("track");
                    break;
                case "accX":
                    //accelerationX = jsonObject.getFloat("accX");
                    break;
                case "accY":
                   // accelerationY = jsonObject.getFloat("accY");
                    break;
                case "accZ":
                   // accelerationZ = jsonObject.getFloat("accZ");
                    break;
                case "speedX":
                    vectorSpeedX = jsonObject.getDouble("speedX");
                    break;
                case "speedY":
                    vectorSpeedY = jsonObject.getDouble("speedY");
                    break;
                case "speedZ":
                    vectorSpeedZ = jsonObject.getDouble("speedZ");
                    break;
                case "latitude":
                    latitude = jsonObject.getDouble("latitude");
                    break;
                case "longitude":
                    longitude = jsonObject.getDouble("longitude");
                    break;
                case "relativeAltitude":
                    //relativeAltitude = jsonObject.getFloat("relativeAltitude");
                    break;
                case "absoluteAltitude":
                    //absoluteAltitude = jsonObject.getFloat("absoluteAltitude");
                    break;
                case "timestamp":
                    timestamp = jsonObject.getLong("timestamp");
                    break;
            }
        }
    }


}

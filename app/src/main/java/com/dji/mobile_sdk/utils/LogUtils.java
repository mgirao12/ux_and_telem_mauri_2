/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.dji.mobile_sdk.utils;

import android.app.Application;
import android.content.Context;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class LogUtils extends Application {

    private static String logFileName;
    private static String logFullFileName;
    private static String jsonFileName;
    private static String jsonFullFileName;
    private static BufferedWriter logWritter;
    private static BufferedWriter jsonWritter;
    private static Boolean silent;
    private static Date flightDate = null;
    private static LogUtils _logUtil = null;
    private static File _file;

    private Application instance;

    @Override
    public Context getApplicationContext() {
        return instance;
    }

    public void setContext(Application application) {
        instance = application;
    }


    public static LogUtils getInstance() {
        if (_logUtil == null) {
            _logUtil = new LogUtils();
        }
        return _logUtil;
    }


    public LogUtils() {

    }


    public void setupLogUtil(String currentDate) {

        _file = getApplicationContext().getExternalFilesDir(null);

        // Create the filename to store the telemetry
        logFileName = "tel-" + currentDate + ".log";
        logFullFileName = _file.getPath() + "/" + logFileName;

        jsonFileName = "json-" + currentDate + ".log";
        jsonFullFileName = _file.getPath() + "/" + jsonFileName;

        silent = false;

        // Create a string writer to collect the test data
        try {
            logWritter = new BufferedWriter(new FileWriter(logFullFileName, true));
            writeInt(logWritter, "Global log file created: " + logFullFileName);
            jsonWritter = new BufferedWriter(new FileWriter(jsonFullFileName, true));
            writeInt(logWritter, "JSON log file created: " + jsonFullFileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setSilent() {
        this.silent = true;
    }

    public void setVerbose() {
        this.silent = false;
    }


    public void close() {

        try {
            logWritter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public synchronized void writeLog(String text) {

        writeInt(logWritter," :: " + text);
    }

    public synchronized void writeLog(String module, String text) {

        writeInt(logWritter,module + " :: " + text);
    }

    public synchronized void writeJSON(String text) {

        writeInt(jsonWritter," :: " + text);
    }

    public synchronized void writeJSON(String module, String text) {

        writeInt(jsonWritter,module + " :: " + text);
    }

    private void writeInt(BufferedWriter writter, String text) {

        try {
            writter.write(System.currentTimeMillis() + " :: " + text + "\n");
            writter.flush();

            if (!silent) System.out.println( text + "\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

package com.dji.mobile_sdk.utils;

public class MessageIDs {
    public static int Telemetry = 0;
    public static int Home = 1;
    public static int Heartbeat = 2;
    public static int ACK = 3;
    public static int Command = 4;
    public static int MissionStatus = 5;
    public static int LTEData = 6;
    public static int DroneStatus = 7;
    public static int SystemStatus = 8;
    public static int RadiationData = 9;
}

package com.dji.mobile_sdk.mqtt.utils;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class CreateNewTopic {
    static BufferedReader br;
    static BufferedWriter out;

    public static void main(String[] args) {
        System.out.println("ADD NEW TOPIC=======================");
        System.out.println("====================================");


        try {
            br = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Enter the new topic name: \n");
            String s = br.readLine();

            Path resourceDirectoryMQTTPubSubFile = Paths.get("core", "src", "main", "resources", "mqtt", "mqttClientPubSubTopics.properties");
            FileWriter fstream = new FileWriter(resourceDirectoryMQTTPubSubFile.toString(), true); //true tells to append data.
            out = new BufferedWriter(fstream);
            out.write("\n");
            out.write("topic_" + s + "=/" + s + "\n");
            out.close();


            File logpath = new File(Paths.get("core", "src", "main", "java", "communications", "mqtt", "listeners").toString() + "/MQTT" + s + "Listener.java");

            if (logpath.createNewFile()) {
                System.out.println(logpath.toString() + " File Created");
            } else System.exit(0);

            String text = "package communications.mqtt.listeners;\n" +
                    "\n" +
                    "import communications.mqtt.MQTTConnection;\n" +
                    "import org.eclipse.paho.client.mqttv3.IMqttMessageListener;\n" +
                    "import org.eclipse.paho.client.mqttv3.MqttMessage;\n" +
                    "\n" +
                    "public class MQTT" + s + "Listener implements IMqttMessageListener {\n" +
                    "\n" +
                    "    private MQTTConnection mqttConnection;\n" +
                    "\n" +
                    "    public MQTT" + s + "Listener() {\n" +
                    "        mqttConnection = MQTTConnection.getInstance();\n" +
                    "    }\n" +
                    "\n" +
                    "    @Override\n" +
                    "    public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {\n" +
                    "        String receivedMessage = new String(mqttMessage.getPayload(), mqttConnection.getENCODING_FOR_PAYLOAD());\n" +
                    "\n" +
                    "    }\n" +
                    "}\n";

            fstream = new FileWriter(logpath.toPath().toString(), false); //true tells to append data.
            out = new BufferedWriter(fstream);
            out.write(text);
            out.close();


            Path fileListenerFactoryPath = Paths.get("core", "src", "main", "java", "communications", "mqtt", "services.flightplan.utils", "ListenerFactory.java");
            logpath = new File(fileListenerFactoryPath.toString());
            List<String> lines = Files.readAllLines(logpath.toPath());
            boolean added = false;
            for (int i = 0; i < lines.size(); i++) {
                lines.get(i).concat("\n");
                if (lines.get(i).contains("default:") && !added) {
                    lines.add(i, "\t\t\t\treturn new MQTT" + s + "Listener();\n");
                    lines.add(i, "\t\t\tcase \"/" + s + "\":\n");
                    added = true;
                    lines.add(3, "import communications.mqtt.listeners.MQTT" + s + "Listener;\n");
                }
            }
            fstream = new FileWriter(fileListenerFactoryPath.toString(), false); //true tells to append data.
            out = new BufferedWriter(fstream);
            for (String line : lines) {
                out.write(line);
            }
            out.close();

            Path fileTopicFactoryPath = Paths.get("core", "src", "main", "java", "communications", "mqtt", "services.flightplan.utils", "TopicFactory.java");
            logpath = new File(fileTopicFactoryPath.toString());
            lines = Files.readAllLines(logpath.toPath());
            added = false;
            for (int i = 0; i < lines.size(); i++) {
                if (lines.get(i).contains("default:") && !added) {
                    lines.add(i, "\t\t\t\treturn baseTopic + vehicle + mqttConfiguration.getString(\"topic_" + s + "\");\n");
                    lines.add(i, "\t\t\tcase \"/" + s + "\":\n");
                    added = true;
                }
            }
            fstream = new FileWriter(fileTopicFactoryPath.toString(), false); //true tells to append data.
            out = new BufferedWriter(fstream);
            for (String line : lines) {
                out.write(line + "\n");
            }

            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

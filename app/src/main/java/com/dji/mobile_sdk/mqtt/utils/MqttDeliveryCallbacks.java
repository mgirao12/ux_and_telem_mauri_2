package com.dji.mobile_sdk.mqtt.utils;


import com.dji.mobile_sdk.EventDispatcherUtils.genericDispatcher.EventDispatcher;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttMessage;


public class MqttDeliveryCallbacks implements MqttCallback {

    EventDispatcher<Throwable> dispatcher;

    public MqttDeliveryCallbacks(EventDispatcher<Throwable> dispatcher) {
        this.dispatcher = dispatcher;
    }

    //Get Project GlobalArea

    /**
     * This method is called when the connection to the server is lost.
     *
     * @param cause the reason behind the loss of connection.
     */
    @Override
    public void connectionLost(Throwable cause) {
        // The MQTT client lost the connection
        cause.printStackTrace();

        dispatcher.dispatch(cause);

    }

    /*
    Se llama cuando se ha completado la entrega de un mensaje y se han recibido todas las confirmaciones.
    Para los mensajes QoS 0 se llama una vez que el mensaje ha sido entregado a la red para su entrega.
    Para QoS 1 se llama cuando se recibe PUBACK y para QoS 2 cuando se recibe PUBCOMP.
    El token será el mismo token que el que se devolvió cuando se publicó el mensaje.
    */
    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {
        // Delivery for a message has been completed
        // and all acknowledgments have been received
    }


    /**
     * This method is called when a message arrives from the server.
     * <p>
     * This method is invoked synchronously by the MQTT client. An
     * acknowledgment is not sent back to the server until this
     * method returns cleanly.
     * <p>
     * If an implementation of this method throws an Exception, then the
     * client will be shut down.  When the client is next re-connected, any QoS
     * 1 or 2 messages will be redelivered by the server.
     * <p>
     * Any additional messages which arrive while an
     * implementation of this method is running, will build up in memory, and
     * will then back up on the network.
     * <p>
     * If an application needs to persist data, then it
     * should ensure the data is persisted prior to returning from this method, as
     * after returning from this method, the message is considered to have been
     * delivered, and will not be reproducible.
     * <p>
     * It is possible to send a new message within an implementation of this callback
     * (for example, a response to this message), but the implementation must not
     * disconnect the client, as it will be impossible to send an acknowledgment for
     * the message being processed, and a deadlock will occur.
     *
     * @param topic   name of the topic on the message was published to
     * @param message the actual message.
     * @throws Exception if a terminal error has occurred, and the client should be
     *                   shut down.
     */
    @Override
    public void messageArrived(String topic, MqttMessage message) {
    }


}

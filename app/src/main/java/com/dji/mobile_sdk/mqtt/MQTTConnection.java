package com.dji.mobile_sdk.mqtt;

import static com.mapbox.mapboxsdk.Mapbox.getApplicationContext;

import android.util.Log;
import android.widget.Toast;

import com.dji.mobile_sdk.EventDispatcherUtils.genericDispatcher.EventDispatcher;
import com.dji.mobile_sdk.PacketsTypes.PacketType;
import com.dji.mobile_sdk.mqtt.utils.MqttDeliveryCallbacks;
import com.dji.mobile_sdk.mqtt.utils.MqttServerConnectionCallbacks;
import com.dji.mobile_sdk.mqtt.utils.MqttSubscriptionCallbacks;
import com.dji.mobile_sdk.mqtt.utils.SecurityTLSHelperFileBased;
import com.dji.mobile_sdk.vehicle_connection.UtilClient;
import com.dji.mobile_sdk.vehicle_connection.UtilServer;
import com.lmax.disruptor.util.Util;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import javax.net.ssl.SSLSocketFactory;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Properties;

public class MQTTConnection {
    private static final Logger log = LogManager.getLogger(MQTTConnection.class);


    private String ENCODING_FOR_PAYLOAD;

    public String getENCODING_FOR_PAYLOAD() {
        return ENCODING_FOR_PAYLOAD;
    }


    private String mqttServerURI;
    private String mqttClientId;
    private InputStream caPath;
    private InputStream certificatePath;
    private InputStream keyPath;
    private String baseTopic;
    public MqttSubscriptionCallbacks mqttSubscriptionCallbacks;
    private EventDispatcher<Boolean> connectionCallbacksEvent = new EventDispatcher<>();
    private EventDispatcher<Throwable> deliveryCallbacks = new EventDispatcher<>();


    private MqttAsyncClient mqttAsyncClient;

    /**
     * Get method to obtain the Mqtt Async Client.
     *
     * @return mqttAsyncClient which is an MqttAsyncClient type.
     */
    public MqttAsyncClient getMqttAsyncClient() {
        return mqttAsyncClient;
    }


    public Configuration mqttPubSubTopicsSettings;


    /**
     * The MqttConnectionOptions class expose those options as simple properties
     * that we can set using normal setter methods. We only need to set the properties
     * required for our scenario – the remaining ones will assume default values.
     */
    private MqttConnectOptions mqttConnectOptions;

    private MqttServerConnectionCallbacks mqttServerConnectionCallbacks;

    public MqttServerConnectionCallbacks getMqttServerConnectionCallbacks() {
        return mqttServerConnectionCallbacks;
    }

    public EventDispatcher<Boolean> getConnectionCallbacksEvent() {
        return connectionCallbacksEvent;
    }

    public EventDispatcher<Throwable> getDeliveryCallbacks() {
        return deliveryCallbacks;
    }


    public MQTTConnection() {
        try {
            // Load al properties in mqttServerSettings instance
            /**
             * Instance to get Configuration file (mqttServerConnection.properties)
             */
            Configurations configPropertiesMqttServer = new Configurations();

            Configuration mqttServerSettings = configPropertiesMqttServer.properties(new File(MQTTConnection.class.getResource("/mqttserverconnection.properties").getFile()));
            //Configuration mqttServerSettings = configPropertiesMqttServer.properties(String.valueOf(new FileInputStream("src/main/assets/mqttserverconnection.properties")));

            // Load al properties in mqttPubSubTopicsSettings instance
            mqttPubSubTopicsSettings = configPropertiesMqttServer.properties(new File(Objects.requireNonNull(MQTTConnection.class.getResource("/mqttclientpubsubtopics.properties")).getFile()));
            //mqttPubSubTopicsSettings = configPropertiesMqttServer.properties(String.valueOf(new FileInputStream(Objects.requireNonNull("src/main/assets/mqttclientpubsubtopics.properties"))));

            // Get MQTT broker address
            //String mqttServerHost = mqttServerSettings.getString("host");
            //String mqttServerHost = UtilServer.getProperty("host", getApplicationContext());
            String mqttServerHost = "rima2.ac.upc.edu";

            // Get MQTT Port, in our case is TLS port
            //int mqttServerPort = mqttServerSettings.getInt("port");
            //int mqttServerPort = Integer.parseInt(UtilServer.getProperty("port", getApplicationContext()));
            int mqttServerPort = 8883;

            // Get ENCODING Topic names, Client ID, User names and Passwords are encoded as  UTF-8 strings
            //ENCODING_FOR_PAYLOAD = mqttServerSettings.getString("encoding_for_payload");
            //ENCODING_FOR_PAYLOAD = UtilServer.getProperty("encoding_for_payload", getApplicationContext());
            //ENCODING_FOR_PAYLOAD = "UTF-8";

            // "The Keep Alive ... is the maximum time interval that is permitted to elapse between the
            // point at which the Client finishes transmitting one Control Packet and the point it starts
            // sending the next. It is the responsibility of the Client to ensure that the interval between
            // Control Packets being sent does not exceed the Keep Alive value. In the absence of sending
            // any other Control Packets, the Client MUST send a PINGREQ Packet."

            //int KEEP_ALIVE_INTERVAL = mqttServerSettings.getInt("mqttServerKeepAlive");
            //int KEEP_ALIVE_INTERVAL = Integer.parseInt(UtilServer.getProperty("mqttServerKeepAlive", getApplicationContext()));
            int KEEP_ALIVE_INTERVAL = 20;

            //This is the maximum number of seconds (chosen by the client) that can elapse without communication
            // between the server and client.
            //int CONNECTION_TIMEOUT = mqttServerSettings.getInt("mqttServerConnectionTimeout");
            //int CONNECTION_TIMEOUT = Integer.parseInt(UtilServer.getProperty("mqttServerConnectionTimeout", getApplicationContext()));
            int CONNECTION_TIMEOUT = 60;

            // Build mqtt broker URI with ssl
            mqttServerURI = String.format("ssl://%s:%d", mqttServerHost, mqttServerPort);

            // Add client identifier to the mqtt connection
            mqttClientId = RandomStringUtils.random(10, true, true);

            // Get certificate names for MQTT TLS connection
            //String caCertificatePath = mqttServerSettings.getString("caCertificateFileName");
           // String clientCertificatePath = mqttServerSettings.getString("clientCertificateFileName");
            //String clientKeyPath = mqttServerSettings.getString("clientKeyFileName");

            //String caCertificatePath = UtilServer.getProperty("caCertificateFileName", getApplicationContext());
            //String clientCertificatePath = UtilServer.getProperty("clientCertificateFileName", getApplicationContext());
            //String clientKeyPath = UtilServer.getProperty("clientKeyFileName", getApplicationContext());

            String caCertificatePath = "ca2.crt";
            String clientCertificatePath = "client.crt";
            String clientKeyPath = "clientkey.key";


            // Absolute path to get MQTT authority certificate file, it has been tested in WIN and linux
            //caPath = MQTTConnection.class.getResourceAsStream("/" + caCertificatePath);
            caPath = new ByteArrayInputStream("C:/Users/Mauri - TFG/AndroidStudioProjects/ux_and_telem_mauri/app/src/main/java/com/dji/mobile_sdk/vehicle_connection/ca2.crt".getBytes());

            // Absolute path to get MQTT crt file, it has been tested in WIN and linux
            //certificatePath = MQTTConnection.class.getResourceAsStream("/" + clientCertificatePath);
            certificatePath = new ByteArrayInputStream("C:/Users/Mauri - TFG/AndroidStudioProjects/ux_and_telem_mauri/app/src/main/java/com/dji/mobile_sdk/vehicle_connection/client.crt".getBytes());
            // Absolute path to get MQTT key file, it has been tested in WIN and linux
            //keyPath = MQTTConnection.class.getResourceAsStream("/" + clientKeyPath);
            keyPath = new ByteArrayInputStream("C:/Users/Mauri - TFG/AndroidStudioProjects/ux_and_telem_mauri/app/src/main/java/com/dji/mobile_sdk/vehicle_connection/clientkey.key".getBytes());

            //Add MQTT Connection Options
            mqttConnectOptions = new MqttConnectOptions();

            // We want to work with MQTT version 3.1.1 interval configuration option.
            mqttConnectOptions.setMqttVersion(MqttConnectOptions.MQTT_VERSION_3_1_1);
            // We want to work with Keep Alive Interval
            mqttConnectOptions.setKeepAliveInterval(KEEP_ALIVE_INTERVAL);
            // We want to add connection timeout
            mqttConnectOptions.setConnectionTimeout(CONNECTION_TIMEOUT);

            // The Paho Java Client provides a pluggable persistence mechanism that makes it possible
            // for the client to store the messages

            //baseTopic = mqttPubSubTopicsSettings.getString("topicBase");
            //baseTopic = UtilClient.getProperty("topicBase", getApplicationContext());
            baseTopic = "data/vehicles/";

        } catch (Exception cex) {
            cex.printStackTrace();
            log.error(cex);
        }
    }

    public void start() {
        try {
            MemoryPersistence memoryPersistence = new MemoryPersistence();

            // Create MQTT Async Client instance, this instance will be used for all UAMA Components
            mqttAsyncClient = new MqttAsyncClient(mqttServerURI, mqttClientId, memoryPersistence);

            SecurityTLSHelperFileBased securityTLSHelperFileBased = new SecurityTLSHelperFileBased();
            SSLSocketFactory socketFactory = securityTLSHelperFileBased.createSocketFactory(caPath,
                    certificatePath, keyPath);

            mqttConnectOptions.setSocketFactory(socketFactory);
            mqttConnectOptions.setAutomaticReconnect(true);
            //Create MQTT connection callbacks
            /**
             * Class to handle connectionLost callback, deliveryComplete callback and messageArrived.
             * The messageArrived callback is one of the most important one because this method is called when a message
             * arrives from the server. This method is invoked synchronously by the MQTT client. An
             * acknowledgment is not sent back to the server until this method returns cleanly.
             */
            MqttDeliveryCallbacks mqttDeliveryCallbacks = new MqttDeliveryCallbacks(deliveryCallbacks);

            // Register callback to MqttAsyncClient such as connectionLost callback, deliveryComplete
            // callback and messageArrived callback.
            mqttAsyncClient.setCallback(mqttDeliveryCallbacks);

            mqttServerConnectionCallbacks = new MqttServerConnectionCallbacks(connectionCallbacksEvent);

            // In this case, we don't use the token
            mqttAsyncClient.connect(mqttConnectOptions, null, mqttServerConnectionCallbacks).waitForCompletion();


        } catch (Exception e) {
            log.error(e);
        }

    }

    public String getTopic(String topic, String vehicle) throws IOException {
        //COMPROBACION
        if (mqttPubSubTopicsSettings == null){
            Log.e("PROPIEDADES MQTT", "ESTA VACIO");
        }
        else{
            Log.e("PROPIEDADES MQTT", "ESTA LLENO");
        }
        switch (topic) {
            case PacketType.Home:
                return baseTopic + vehicle + mqttPubSubTopicsSettings.getString("topic_home");
                //return baseTopic + vehicle + UtilClient.getProperty("topic_home", getApplicationContext());
            case PacketType.Telemetry:
                //return baseTopic + vehicle + mqttPubSubTopicsSettings.getString("topic_telemetry");
                //return baseTopic + vehicle + UtilClient.getProperty("topic_telemetry", getApplicationContext());
                return baseTopic + vehicle + "/telemetry";
            case PacketType.ACK:
                return baseTopic + vehicle + mqttPubSubTopicsSettings.getString("topic_ack");
            case PacketType.Command:
                return baseTopic + vehicle + mqttPubSubTopicsSettings.getString("topic_Command");
            case PacketType.Heartbeat:
                return baseTopic + mqttPubSubTopicsSettings.getString("topic_heartbeat");
            case PacketType.LTEData:
                return baseTopic + vehicle + mqttPubSubTopicsSettings.getString("topic_4GData");
            case PacketType.DroneStatus:
                return baseTopic + vehicle + mqttPubSubTopicsSettings.getString("topic_status");
            case "JVMInfo":
                return baseTopic + vehicle + mqttPubSubTopicsSettings.getString("topic_JVM");
            case "systemInfo":
                return baseTopic + vehicle + mqttPubSubTopicsSettings.getString("topic_system");
            case PacketType.MissionStatus:
                return baseTopic + vehicle + mqttPubSubTopicsSettings.getString("topic_missionStatus");
            case PacketType.SystemStatus:
                return baseTopic + vehicle + mqttPubSubTopicsSettings.getString("topic_systemStatus");
            case PacketType.RadiationData:
                return baseTopic + vehicle + mqttPubSubTopicsSettings.getString("topic_radiationData");

            default:
                return null;
        }
    }

    public void setMqttClientId(String mqttClientId) {
        this.mqttClientId = mqttClientId;
    }

    public String getMqttClientId() {
        return this.mqttClientId;
    }

    public synchronized IMqttDeliveryToken publishMessage(final String topic, final String textForMessage, IMqttActionListener actionListener,
                                                          final int qos, final boolean retained) {
        byte[] bytesForPayload;
        Log.e("MQTT:", "dentro de PUBLISHMESSAGE");
        Log.e("caPath", String.valueOf(caPath));
        Log.e("certificatePath", String.valueOf(certificatePath));
        Log.e("keyPath", String.valueOf(keyPath));
        try {
            Log.e("MQTT:", "dentro de TRY");
            Log.e("textForMessage ", textForMessage);
            //Log.e("encoding payload ", ENCODING_FOR_PAYLOAD);
            //bytesForPayload = textForMessage.getBytes(ENCODING_FOR_PAYLOAD);
            bytesForPayload = textForMessage.getBytes("UTF-8");

            return mqttAsyncClient.publish(
                    topic, bytesForPayload, qos, retained, null, actionListener);

        } catch (UnsupportedEncodingException | MqttException e) {
            log.error(e);
            Log.e("MQTT:", "dentro de CATCH");
            Log.e("MQTT:", "NOT connected, token: " + e);
            return null;

        }
    }

    private List<IMqttToken> mqttListenersList = new ArrayList<>();

    public List<IMqttToken> getMqttListenersList() {
        return mqttListenersList;
    }


    public MqttSubscriptionCallbacks getMqttSubscriptionCallbacks() {
        return mqttSubscriptionCallbacks;
    }

}

package com.dji.mobile_sdk.mqtt.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bouncycastle.util.io.pem.PemObject;
import org.bouncycastle.util.io.pem.PemReader;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.TrustManagerFactory;
import java.io.*;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;


public class SecurityTLSHelper {
    private static final Logger log = LogManager.getLogger(SecurityTLSHelper.class);

    private final static String TLS_VERSION = "TLSv1.2";

    //This returns a java.key.KeyFactory instance that converts public/private keys of the RSA algorithm.
    private static KeyFactory getKeyFactoryInstance() throws NoSuchAlgorithmException {
        return KeyFactory.getInstance("RSA");
    }

    /*
    This method receives a certificate file name, loads its contents, generates an instance of
    java.security.cert.X509Certificate from this file, and returns the created instance. Other methods
    will call the createX509CertificateFromFile method to generate an X509Certificate instance from
    a certificate authority certificate file or from a client certificate file.
    */
    private static X509Certificate createX509CertificateFromFile(final String certificateFileName)
            throws IOException, CertificateException {
        // Load an X509 ca certificate from the specified certificate file name
        final File file = new File(certificateFileName);
        if (!file.isFile()) {
            throw new IOException(String.format("The certificate file %s doesn't exist.", certificateFileName));
        }

        final CertificateFactory certificateFactoryX509 = CertificateFactory.getInstance("X.509");
        final InputStream inputStream = new FileInputStream(file);
        final X509Certificate certificate = (X509Certificate) certificateFactoryX509.generateCertificate(inputStream);

        inputStream.close();

        return certificate;
    }

    //This method receives a key file name in the PEM format, loads its contents, generates an instance
    // of java.Security.PrivateKey from this file, and returns the created instance.
    //This method will allow us to create a PrivateKey instance from the client key file.
    private static PrivateKey createPrivateKeyFromPemFile(final String keyFileName)
            throws IOException, InvalidKeySpecException, NoSuchAlgorithmException {
        // Loads a private key from the specified key file name
        final PemReader pemReader = new PemReader(new FileReader(keyFileName));
        final PemObject pemObject = pemReader.readPemObject();
        final byte[] pemContent = pemObject.getContent();
        pemReader.close();
        final PKCS8EncodedKeySpec encodedKeySpec = new PKCS8EncodedKeySpec(pemContent);
        final KeyFactory keyFactory = getKeyFactoryInstance();
        return keyFactory.generatePrivate(encodedKeySpec);
    }

    /*
    This method receives a client certificate file name, a client key file name, and a client key password,
    and calls the previously explained createX509CertificateFromFile and createPrivateKeyFromPemFile methods
    to generate the X509Certificate and PrivateKey instances. Then, the method uses these instances
    to specify the certificate and key for an instance of the java.security.KeyStore instance and uses
    it to create and return an instance of the java.net.ssl.KeyManagerFactory class.
     */

    private static KeyManagerFactory createKeyManagerFactory(final String clientCertificateFileName,
                                                             final String clientKeyFileName)
            throws InvalidKeySpecException, NoSuchAlgorithmException, KeyStoreException,
            IOException, CertificateException, UnrecoverableKeyException {
        // This code calls the previously explained createX509CertificateFromFile method with
        // clientCertificateFileName as an argument to load the client certificate, generate the X509Certificate
        // instance, and save its reference in the clientCertificate variable.
        final X509Certificate clientCertificate = createX509CertificateFromFile(clientCertificateFileName);

        // createPrivateKeyFromPemFile method with clientKeyFileName as an argument to load the private client
        // key with PEM format, generate the PrivateKey instance, and save its reference in the privateKey variable.
        final PrivateKey privateKey = createPrivateKeyFromPemFile(clientKeyFileName);

        // This code creates an instance of the KeyStore class named keyStore, whose goal is to provide storage
        // facility for cryptographic keys and certificates.
        final KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
        keyStore.load(null, null);

        // The call to the keyStore.setCertificateEntry method assigns the trusted certificate saved in the
        // clientCertificate variable to the "certificate" alias.
        keyStore.setCertificateEntry("certificate", clientCertificate);

        // The call to the keyStore.setKeyEntry method assigns the key saved in the privateKey variable to the
        // "private-key" alias, sets its association with the certificate saved in clientCertificate, and protects
        // it with the password received in the clientKeyPassword argument.
        keyStore.setKeyEntry("private-key", privateKey, "".toCharArray(),
                new Certificate[]{clientCertificate});

        // Next code creates an instance of the KeyManagerFactory class and then calls its init method with
        // keyStore and clientKeyPassword converted to a char array as the arguments to provide the KeyStore
        // instance and the password to the key manager factory.
        final KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
        keyManagerFactory.init(keyStore, "".toCharArray());

        // The code returns the created and initialized key manager factory.
        return keyManagerFactory;
    }

    /*
    This method receives a certificate authority certificate file name and calls the previously explained
    createX509CertificateFromFile method to generate an X509Certificate instance. Then, the method uses this
    instance to specify the certificate authority certificate for an instance of the java.security.KeyStore instance
    and uses it to create and return an instance of the java.net.ssl.TrustManagerFactory class.
   */
    private static TrustManagerFactory createTrustManagerFactory(
            final String caCertificateFileName)
            throws CertificateException, NoSuchAlgorithmException, IOException, KeyStoreException {
        // Creates a trust manager factory
        // Load CA certificate
        final X509Certificate caCertificate = (X509Certificate) createX509CertificateFromFile(caCertificateFileName);
        // CA certificate is used to authenticate server
        // The next line creates an instance of the KeyStore class named keyStore, whose goal is to provide storage
        // facility for cryptographic keys and certificates.
        final KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
        keyStore.load(null, null);

        // The call to the keyStore.setCertificateEntry method assigns the trusted certificate authority
        // certificate saved in the caCertificate variable to the "ca-certificate" alias.
        keyStore.setCertificateEntry("ca-certificate", caCertificate);

        // Next code creates an instance of the TrustManagerFactory class and then calls its init method with
        // keyStore as an argument to provide the KeyStore instance and to the trust manager factory.
        final TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        trustManagerFactory.init(keyStore);

        // The code returns the created and initialized trust manager factory.
        return trustManagerFactory;
    }
}

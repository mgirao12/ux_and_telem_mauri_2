package com.dji.mobile_sdk.mqtt.utils;


import com.dji.mobile_sdk.EventDispatcherUtils.genericDispatcher.EventDispatcher;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;

public class MqttServerConnectionCallbacks implements IMqttActionListener {

    private static final Logger logger = LogManager.getLogger(MqttServerConnectionCallbacks.class);

    private EventDispatcher<Boolean> dispatcher;

    public MqttServerConnectionCallbacks(EventDispatcher<Boolean> dispatcher) {
        this.dispatcher = dispatcher;
    }

    /**
     * Implementors of IMqttActionListener interface will be notified when an asynchronous action completes.
     *
     * <p>A listener is registered on an MqttToken and a token is associated
     * with an action like connect or publish. When used with tokens on the MqttAsyncClient
     * the listener will be called back on the MQTT client's thread. The listener will be informed
     * if the action succeeds or fails. It is important that the listener returns control quickly
     * otherwise the operation of the MQTT client will be stalled.
     * </p>
     */

    /**
     * This method is invoked when an action has completed successfully.
     *
     * @param asyncActionToken associated with the action that has completed
     */
    @Override
    public void onSuccess(IMqttToken asyncActionToken) {
        // the onSuccess method will be executed when the connection has
        // been successfully established with the MQTT server.
        logger.debug("Successfully connected");
        dispatcher.dispatch(true);

        /**
         synchronized (MQTTConnection.getInstance().getMqttSubscriber()) {
         MQTTConnection.getInstance().getMqttSubscriber().notifyAll();
         }**/
    }

    /**
     * This method is invoked when an action fails.
     * If a client is disconnected while an action is in progress
     * onFailure will be called. For connections
     * that use cleanSession set to false, any QoS 1 and 2 messages that
     * are in the process of being delivered will be delivered to the requested
     * quality of service next time the client connects.
     *
     * @param asyncActionToken associated with the action that has failed
     */
    @Override
    public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
        // This method is fired when an operation failed
        logger.error(exception);
        dispatcher.dispatch(false);
    }

}

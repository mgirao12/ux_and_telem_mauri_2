package com.dji.mobile_sdk.mqtt.utils;

import com.dji.mobile_sdk.EventDispatcherUtils.genericDispatcher.EventDispatcher;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;

public class MqttSubscriptionCallbacks implements IMqttActionListener {
    private static final Logger logger = LogManager.getLogger(MqttSubscriptionCallbacks.class);

    private EventDispatcher<IMqttToken> dispatcher;

    public MqttSubscriptionCallbacks(EventDispatcher<IMqttToken> dispatcher) {
        this.dispatcher = dispatcher;
    }

    @Override
    public void onSuccess(IMqttToken asyncActionToken) {
        logger.debug(String.format("Subscribed to the %s topic with QoS: %d",
                asyncActionToken.getTopics()[0], asyncActionToken.getGrantedQos()[0]));
        dispatcher.dispatch(asyncActionToken);
    }


    @Override
    public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
        logger.error(exception);
    }

}

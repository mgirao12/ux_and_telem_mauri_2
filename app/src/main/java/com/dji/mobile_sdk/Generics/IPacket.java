package com.dji.mobile_sdk.Generics;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public interface IPacket {
    byte[] pack() throws IOException;

    void unpack(byte[] data) throws IOException;

    JSONObject toJSON() throws JSONException;

    int getMsgID();
}

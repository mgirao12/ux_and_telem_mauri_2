package com.dji.mobile_sdk.Generics;

import org.json.JSONException;

public interface IDataPacket extends IPacket {
    void fromString(String string) throws JSONException;
    void setSystemId(String string);

}

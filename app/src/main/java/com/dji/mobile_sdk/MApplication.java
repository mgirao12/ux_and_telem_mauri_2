package com.dji.mobile_sdk;

import android.app.Application;
import android.content.Context;


import com.dji.mobile_sdk.utils.LogUtils;
import com.dji.mobile_sdk.vehicle_connection.SDKHandler;
import com.secneo.sdk.Helper;

import java.util.Date;

public class MApplication extends Application {
    private SDKHandler sdkHandler;

    private static LogUtils myLog;

    @Override
    protected void attachBaseContext(Context paramContext) {
        super.attachBaseContext(paramContext);
        Helper.install(MApplication.this);

        myLog = LogUtils.getInstance();
        myLog.setContext(this);
        Date today = new Date();
        myLog.setupLogUtil(String.valueOf(today.getTime()));
        myLog.setSilent();

        if (sdkHandler == null) {
            sdkHandler = new SDKHandler();
            sdkHandler.setContext(this);
        }

    }

    @Override
    public void onCreate() {
        super.onCreate();
        sdkHandler.onCreate();
    }
}
